package see.testicle.api;

import java.io.File;
import java.util.Objects;

public interface SeeTestSupportApi {
    String RESOURCES = "resources/";

    default File getFile(String path) {
        File file = getFileByPath(path);
        if (file.isDirectory()) {
            throw new RuntimeException("It's a dir %s".formatted(file.getAbsolutePath()));
        }
        if (!file.exists()) {
            throw new RuntimeException("File doesnt' exist %s".formatted(file.getAbsolutePath()));
        }
        return file;
    }

    private static String getFilepathFromResources(String path) {
        return Objects.requireNonNull(SeeTestSupportApi.class.getClassLoader().getResource(path)).getFile();
    }

    @Deprecated(since = "Wrong path")
    private static String getResourcesPath() {
        return Objects.requireNonNull(SeeTestSupportApi.class.getClassLoader().getResource(""))
                .toExternalForm() + RESOURCES;
    }

    private static File getFileByPath(String path) {
        String resourcePath = getFilepathFromResources(path);
        return new File(resourcePath);
    }

    default String getTestRecoursesDir(Class<?> clazz) {
        return clazz.getCanonicalName().replaceAll("\\.", "/") + "/";
    }
}
